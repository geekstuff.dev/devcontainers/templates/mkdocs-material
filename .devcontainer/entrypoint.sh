#!/usr/bin/env bash

# https://code.visualstudio.com/remote/advancedcontainers/start-processes#_adding-startup-commands-to-the-docker-image-instead

set -e

echo "Entrypoint starting"

if test -e ${CONTAINER_WORKSPACE_FOLDER}/requirements.txt; then
    echo "Pre-installing requirements.txt (in the background)"
    echo "  output will be available at /tmp/pip3-install-requirements.log"

    PIP_INDEX_OPTION=""
    if test -n "$PIP_INDEX_URL"; then
        PIP_INDEX_OPTION="-i $PIP_INDEX_URL"
    fi

    pip3 --disable-pip-version-check install $PIP_INDEX_OPTION -r ${CONTAINER_WORKSPACE_FOLDER}/requirements.txt \
        | tee -a /tmp/pip3-install-requirements.log
    touch /tmp/pip3-requirements-installed
fi

exec "$@"
