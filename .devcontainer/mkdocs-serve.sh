#!/bin/sh

while true; do
    # ensure dockerfile entrypoint pip3 command is completed
    if test -e requirements.txt && ! test -e /tmp/pip3-requirements-installed; then
        echo "Waiting for pip3 requirements installation to complete."
        echo "  (details at /tmp/pip3-install-requirements.log)"
        sleep 2
        continue
    fi

    # serve or sleep and continue
    if test -e mkdocs.yml; then
        mkdocs serve --dev-addr=127.0.0.1:8000 || true
    else
        echo "error: no mkdocs.yml found. refusing to start"
    fi

    echo "trying again in 3 seconds"
    sleep 3
 done
