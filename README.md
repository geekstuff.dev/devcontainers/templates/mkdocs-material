# Devcontainers Templates - mkdocs-material

This project is a Devcontainer built on top of the [squidfunk/mkdocs-material](https://squidfunk.github.io/mkdocs-material/)
docker image, with extensions, startup script, settings, all pre-build in a single
docker image to include in your own devcontainer.json

## How it's used

The .devcontainer/ configuration present in this project can be used with a single
entry in your own `.devcontainer/devcontainer.json` file:

```json
{
    "name": "x-project-name",
    "image": "registry.gitlab.com/geekstuff.dev/devcontainers/templates/mkdocs-material"
}
```

## What's included on top

- Non-root dev user
- Auto-pre installing of python dependencies (ON BUILD, to validate)
- Auto-startup of the `mkdocs serve` command
- VSCode python extensions and basic settings
